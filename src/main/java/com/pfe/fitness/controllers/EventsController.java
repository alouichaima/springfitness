package com.pfe.fitness.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pfe.fitness.entities.Events;
import com.pfe.fitness.payload.response.MessageResponse;
import com.pfe.fitness.repository.EventsRepository;
import com.pfe.fitness.services.EventsService;
import com.pfe.fitness.services.UtilisateurServices;


@RestController
@RequestMapping("/events") // localhost:8086/events

public class EventsController {
	
	@Autowired
	private EventsService eventsService;
	
	
	
	@Autowired  
	ServletContext context;

	
	
	//public static String uploadDirectory = System.getProperty("user.dir") + "/src/main/webapp/imagedata";

	
	@GetMapping(path = "/all") //localhost:8087/events/all
	public List<Events> getAllEvents() {
		return eventsService.getAllEvents();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Events>  findEventsById (@PathVariable Long id) {
		Events events = eventsService.findEventsById(id);
		if (events==null) {
			return new ResponseEntity<Events>(HttpStatus.NO_CONTENT);
		}else {
			return new ResponseEntity<Events>(events, HttpStatus.OK);
		}
	}
	
	@PostMapping
	public Events createEvents(@RequestBody Events events) {
		return eventsService.createEvents(events);
	}
	
	@PutMapping
	public Events updateEvents(@RequestBody Events events) {
		return eventsService.updateEvents(events);
				
	}
/*	@DeleteMapping(path= "/{id}") 
	public void deleteUtilisateur(@PathVariable Long id) {
		 eventsService.deleteEvents(id);
	}*/
	@DeleteMapping("/delete/{NomE}")
	public String deleteEtudiant(@PathVariable String NomE)
    {	
		 return eventsService.deleteEvents(NomE);
    }
	
	
	/*@RequestMapping("addEvent")
	@ResponseBody
	public String saveEvents(Events even,@RequestParam("img") MultipartFile file) {
		
		StringBuilder fileNames = new StringBuilder();
		String filename=even.getId() + file.getOriginalFilename().substring(file.getOriginalFilename().length()-4);
		Path fileNameAndPath =Paths.get(uploadDirectory,filename);
		try {
			Files.write(fileNameAndPath, file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		even.setImage(filename);
		repo.save(even);
		return "Save Data Successfully ! ";
	}*/
	
	/* @PostMapping("/eventss")
	 public long createEvents (@RequestParam("file") MultipartFile file,
			 @RequestParam("events") String events) throws JsonParseException , JsonMappingException , Exception
	 {
		 System.out.println("save Events succ");
       Events ev = new ObjectMapper().readValue(events, Events.class);
       boolean isExit = new File(context.getRealPath("/Images/")).exists();
       if (!isExit)
       {
       	new File (context.getRealPath("/Images/")).mkdir();
       	System.out.println("mk dir Images ");
       }
       String filename = file.getOriginalFilename();
       String newFileName = FilenameUtils.getBaseName(filename)+"."+FilenameUtils.getExtension(filename);
       File serverFile = new File (context.getRealPath("/Images/"+File.separator+newFileName));
       try
       {
       	System.out.println("Image");
       	 FileUtils.writeByteArrayToFile(serverFile,file.getBytes());
       	 
       }catch(Exception e) {
       	e.printStackTrace();
       }

      
       ev.setFileName(newFileName);
       return eventsService.save(ev);

}
	 
	 @GetMapping(path="/Imgevents/{id}")
	 public byte[] getPhoto(@PathVariable("id") Long id) throws Exception{
		Events Events = eventsService.findEventsById(id);
		 return Files.readAllBytes(Paths.get(context.getRealPath("/Images/")+Events.getFileName()));
	 }*/
	 


}
