package com.pfe.fitness.services;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.pfe.fitness.entities.Events;
import com.pfe.fitness.payload.response.MessageResponse;

public interface EventsService {
	public List<Events> getAllEvents();
	public Events findEventsById (Long id);
	public Events createEvents( Events events);
	public Events updateEvents( Events events);
	public void deleteEvents( long id);
	
	
	public long save(Events ev);
	public String deleteEvents(String nomE);
	

}
